#**Behavioral Cloning** 


**Behavioral Cloning Project**

The goals / steps of this project are the following:
* Use the simulator to collect data of good driving behavior
* Build, a convolution neural network in Keras that predicts steering angles from images
* Train and validate the model with a training and validation set
* Test that the model successfully drives around track one without leaving the road
* Summarize the results with a written report


## Rubric Points
###Here I will consider the [rubric points](https://review.udacity.com/#!/rubrics/432/view) individually and describe how I addressed each point in my implementation.  

---
###Files Submitted & Code Quality

####1. Submission includes all required files and can be used to run the simulator in autonomous mode

My project includes the following files:
* model.py containing the script to create and train the model
* drive.py for driving the car in autonomous mode
* model.h5 containing a trained convolution neural network 
* writeup_report.md or writeup_report.pdf summarizing the results

####2. Submission includes functional code
To create the model, the file model.py has to be run as:
```sh
python model.py
```
Using the Udacity provided simulator and my drive.py file, the car can be driven autonomously around the track by executing 
```sh
python drive.py model.h5
```
The model has been trained with images from the simulator.

####3. Submission code is usable and readable

The model.py file contains the code for training and saving the convolution neural network. The file shows the pipeline I used for training and validating the model, and it contains comments to explain how the code works.

###Model Architecture and Training Strategy

####1. An appropriate model architecture has been employed

My model consists of a convolution neural network with 3x3 filter sizes and 5x5 filter sizes and depths between 24 and 64 (model.py lines 91-95) 

The model includes RELU layers to introduce nonlinearity (code lines 91-95 and 98-100), and the data is normalized in the model using a Keras lambda layer (code line 90). 

Towards the end, there are fully connected layers to adapt to the data.

####2. Attempts to reduce overfitting in the model

The model contains dropout layers in order to reduce overfitting (model.py line 96). 

The model was trained and validated on different data sets to ensure that the model was not overfitting (code line 56-67). The model was tested by running it through the simulator and ensuring that the vehicle could stay on the track.

####3. Model parameter tuning

The model used an adam optimizer, so the learning rate was not tuned manually (model.py line 102).

####4. Appropriate training data

Training data was chosen to keep the vehicle driving on the road. I used a combination of center lane driving, recovering from the left and right sides of the road as the vehicle deviates from the center. 

For details about how I created the training data, see the next section. 

- [x] Track1
- [ ] Track2

###Model Architecture and Training Strategy

####1. Solution Design Approach

The overall strategy for deriving a model architecture was to use steering angles to correct the driving of the car on the road.

My first step was to use a convolution neural network model similar to the LeNet model used in the previous project. I thought this model might be appropriate because it provides a good starting point to see how the model trains and behaves in the autonomous mode.

In order to gauge how well the model was working, I split my image and steering angle data into a training and validation set. I found that my first model had a low mean squared error on the training set but a high mean squared error on the validation set. This implied that the model was overfitting. 

To combat the overfitting, I modified the model so that there are dropout layer included. Additionally the epochs were reduced as overfitting becomes visible at higher epochs.

Then I added the fully connected layers to match the desired output. Further it is compiled with mean square error as this is a regression problem as opposed to classification problem in case of traffic sign classifier. 

The final step was to run the simulator to see how well the car was driving around track one. There were a few spots where the vehicle fell off the track near the brdige. In order to improve the driving behavior in these cases, I included images from all 3 cameras to provide the appropriate angle to turn when it goes off course.

At the end of the process, the vehicle is able to drive autonomously around the track without leaving the road.

####2. Final Model Architecture

The final model architecture (model.py lines 18-24) consisted of a convolution neural network with the following layers and layer sizes:

- Image Cropping
- Image normalization
- Convolution: 5x5, filter: 24, strides: 2x2, activation: RELU
- Convolution: 5x5, filter: 36, strides: 2x2, activation: RELU
- Convolution: 5x5, filter: 48, strides: 2x2, activation: RELU
- Convolution: 3x3, filter: 64, strides: 1x1, activation: RELU
- Convolution: 3x3, filter: 64, strides: 1x1, activation: RELU
- Drop out (0.5)
- Fully connected: neurons: 100, activation: RELU
- Fully connected: neurons: 50, activation: RELU
- Fully connected: neurons: 10, activation: RELU
- Fully connected: neurons: 1 (output)



####3. Creation of the Training Set & Training Process

To capture good driving behavior, I first recorded two laps on track one using center lane driving. The typical camera image of the track from the training can be seen as:

**Center Image**

![Center Image](examples/center.jpg)

I then recorded the vehicle recovering from the left side and right sides of the road back to center so that the vehicle would learn to recover from any deviations from the center of the road. 
Track 2 was not done in view of project 3 timeline. It will be added soon.

Before the data could be preprocessed, the image is cropped to remove the unnecessary portions such as the car bonet and the trees. This way the training is focussed on the road. The cropped image can be seen as:

**Cropped Image**

![Cropped Image](examples/cropped.png)


To augment the data sat, I also flipped images and angles thinking that this would provide the car the necessary angle to turn when it is moving away from the center of the road. 

After the collection process, I had sufficient data points. I then preprocessed this data by by cropping and normalization within the model.


I finally randomly shuffled the data set and put Y% of the data into a validation set. 

I used this training data for training the model. The validation set helped determine if the model was over or under fitting. The ideal number of epochs was Z as evidenced by ... I used an adam optimizer so that manually training the learning rate wasn't necessary.
