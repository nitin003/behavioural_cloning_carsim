import numpy as np
from sklearn.model_selection import train_test_split
from keras.models import Sequential
from keras.layers import Lambda, Conv2D, Cropping2D
from keras.layers.core import Dense, Activation, Flatten, Dropout, Reshape
#from keras.layers.pooling import MaxPooling2D
from keras.backend import tf as ktf
import csv, argparse, cv2, sklearn, threading
import matplotlib.pyplot as plt
from keras import backend as K
#from keras.regularizers import l2
K.set_image_dim_ordering('tf')


class Model():
    ''''
        Class implementing the loading the images, actual model and training the model.
    '''
    model = Sequential()
    lines = []
    images = []
    measurements = []
    augmented_images = []
    augmented_measurements = []
    nb_epoch = 2
    keep_prob = 0.5
    batch_size = 40

    def __init__(self):
       pass

    ''' Load the data from the csv file into an array. '''
    def load_data(self):
        with open('./data/driving_log.csv') as csvfile:
            reader = csv.reader(csvfile)
            for line in reader:
                self.lines.append(line)
        #return self.lines[1:]

    ''' Here we pre-process the image by adding sufficient data for the model.
        Additionally normalize and remove unnecessary areas of the image. '''
    #@threadsafe_generator
    def preprocessor_generator(self):
        samples = self.lines[1:]
        num_samples = len(samples)
        image_group = []
        measurement_group = []
        correction = 0.2
        #while True:  # Loop forever so the generator never terminates
        sklearn.utils.shuffle(samples)
        for offset in range(0, num_samples):
            #batch_samples = samples[offset:offset + batch_size]
            batch_sample = samples[offset]
            choose_cam = np.random.choice(3)
            #for batch_sample in samples:
            for i in range(3):
                name = './data/IMG/' + batch_sample[i].split('/')[-1]
                temp = cv2.imread(name)
                image = cv2.cvtColor(temp, cv2.COLOR_BGR2RGB)
                # Crop the image to remove the car bonnet and the sky
                if image is not None:
                    self.images.append(image)
                if i>0:
                    center_angle = float(batch_sample[3]) + (correction * (-1 if (i%2==0) else 1))
                else:
                    center_angle = float(batch_sample[3])
                self.measurements.append(center_angle)
            #self.images.append(image_group[choose_cam])
            #self.measurements.append(measurement_group[choose_cam])
        for image, measurement in zip(self.images, self.measurements):
            self.augmented_images.append(image)
            self.augmented_images.append(cv2.flip(image, 1))
            self.augmented_measurements.append(measurement)
            self.augmented_measurements.append(measurement * (-1.0))
        X_train = np.array(self.augmented_images)
        y_train = np.array(self.augmented_measurements)
        return X_train, y_train
            # trim image to only see section with road
        #X_train = np.array(self.augmented_images)
        #y_train = np.array(self.augmented_measurements)
        #return X_train, y_train
                #yield sklearn.utils.shuffle(X_train, y_train)


    ''' Adding the actual model here. Model used is from the NVIDIA white paper on
        End-to-End learning for Self-Driving Cars '''
    def build_train_model(self, X_train, y_train):
        # compile and train the model using the generator function
        self.model.add(Cropping2D(cropping=((70,25), (0,0)), input_shape=(160, 320, 3)))
        self.model.add(Lambda(lambda x: x / 127.5 - 1))
        self.model.add(Conv2D(24, 5, 5, subsample=(2, 2), activation='relu'))
        self.model.add(Conv2D(36, 5, 5, subsample=(2, 2), activation='relu'))
        self.model.add(Conv2D(48, 5, 5, subsample=(2, 2), activation='relu'))
        self.model.add(Conv2D(64, 3, 3, activation='relu'))
        self.model.add(Conv2D(64, 3, 3, activation='relu'))
        self.model.add(Dropout(self.keep_prob))
        self.model.add(Flatten())
        self.model.add(Dense(100, activation='relu'))
        self.model.add(Dense(50, activation='relu'))
        self.model.add(Dense(10, activation='relu'))
        self.model.add(Dense(1))
        self.model.compile(loss='mse', optimizer='adam')
        history_object = self.model.fit(X_train, y_train, validation_split=0.2, shuffle=True, nb_epoch=self.nb_epoch)
        # history_object = self.model.fit_generator(train_generator, samples_per_epoch=len(train_samples), \
        #                          validation_data=validation_generator, \
        #                          nb_val_samples=len(validation_samples), nb_epoch=3)
        plt.plot(history_object.history['loss'])
        plt.plot(history_object.history['val_loss'])
        plt.title('model mean squared error loss')
        plt.ylabel('mean squared error loss')
        plt.xlabel('epoch')
        plt.legend(['training set', 'validation set'], loc='upper right')
        plt.show()
        self.model.save('model.h5')

''' Program starts Here '''
def main():
    ''' All the methods are called from the main function '''
    parser = argparse.ArgumentParser(description='Project 3 - Behavioural Cloning')
    parser.add_argument('-n', help='number of epochs',      dest='self.nb_epoch',          type=int,   default=10)
    parser.add_argument('-b', help='batch size',            dest='self.batch_size',        type=int,   default=40)
    args = parser.parse_args()

    print('-' * 30)
    print('Parameters')
    print('-' * 30)
    for key, value in vars(args).items():
        print('{:<20} := {}'.format(key, value))
    print('-' * 30)

    # Create the model, build and train it
    obj = Model()
    samples = obj.load_data()    #load_data(args)
    #train_samples, validation_samples = train_test_split(samples, test_size=0.2)
    X_train, y_train = obj.preprocessor_generator()
    obj.build_train_model(X_train, y_train)


if __name__ == '__main__':
    main()